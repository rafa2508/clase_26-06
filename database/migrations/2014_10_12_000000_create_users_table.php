<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password');
            $table->integer('computer_id');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('computer_id')->references('id')->on('computers');
        });

        Schema::create('roles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
    });

        Schema::create('user_roles', function (Blueprint $table) {
           $table->integer('user_id')->unsigned();
           $table->integer('role_id')->unsigned();
           $table->foreign('user_id')->references('id')->on('users');
           $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::drop('roles');
        Schema::drop('user_roles');
    }
}
