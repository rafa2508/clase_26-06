<?php

use Illuminate\Database\Seeder;

class ComputerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Computer::create([
            "brand" => "HP",
            "model" => "360xEnvy",
            "date" => "2019-01-15"
        ]);

        \App\Models\Computer::create([
            "brand" => "HP",
            "model" => "Pavillion",
            "date" => "2018-09-05"
        ]);

    }
}
