<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::create([
            "name" => "Director"
        ]);

        \App\Models\Role::create([
            "name" => "Profesor"
        ]);

        \App\Models\Role::create([
            "name" => "Estudiante"
        ]);
    }
}
