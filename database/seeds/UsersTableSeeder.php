<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "username" => "admin",
            "password" => Hash::make('admin')
        ]);

        User::create([
            "username" => "user",
            "password" => Hash::make('user')
        ]);
    }
}
